package pactverification;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.StateChangeAction;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.junitsupport.loader.PactBrokerAuth;
import au.com.dius.pact.provider.junitsupport.loader.SelectorBuilder;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Tag("pact")
@Provider("ArticlesProvider")
@PactBroker(url = "https://kiran.pactflow.io", authentication = @PactBrokerAuth(token = "BgTq1hNVUtN4ye2Eldw-iQ"))
public class ProviderTest {

    WireMockServer wireMockServer;

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @BeforeEach
    void before(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", 8080, "/"));
        System.setProperty("pact.verifier.publishResults", "true");
        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();
    }

    @au.com.dius.pact.provider.junitsupport.loader.PactBrokerConsumerVersionSelectors
    public static SelectorBuilder consumerVersionSelectors() {
        return new SelectorBuilder()
                // .mainBranch() // (recommended) - Returns the pacts for consumers configured mainBranch property
                 .deployedOrReleased();  // (recommended) - Returns the pacts for all versions of the consumer that are currently deployed or released and currently supported in any environment.
                //.deployedTo("test") // Normally, this would not be needed, Any versions currently deployed to the specified environment.
                //.deployedTo("production") // Normally, this would not be needed, Any versions currently deployed to the specified environment.
               // .environment("dev")
                //.environment("test") // Normally, this would not be needed, Any versions currently deployed or released and supported in the specified environmen
        //.environment("production"); // Normally, this would not be needed, Any versions currently deployed or released and supported in the specified environment.
    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    @State(value = "Articles exist", action = StateChangeAction.SETUP)
    public void articlesExist() {
        wireMockServer.stubFor(get(urlEqualTo("/articles.json"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/articles.json")));
    }

    @State(value = "Articles exist", action = StateChangeAction.TEARDOWN)
    public void articlesExistTearDown() {
    }

    @State(value = "No articles exist", action = StateChangeAction.SETUP)
    public void noArticlesExist() {
        wireMockServer.stubFor(get(urlEqualTo("/articles.json"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(404)));
    }

    @State(value = "No articles exist", action = StateChangeAction.TEARDOWN)
    public void noArticlesExistTearDown() {
    }


}
